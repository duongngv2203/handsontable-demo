import { Component, ViewChild } from '@angular/core';
import Handsontable from 'handsontable/base';
import { ContextMenu } from 'handsontable/plugins/contextMenu';

type InfoTable = {
  ID: string,
  name: string,
  age: string,
  sex: string,
  address: string,
  relationship: boolean
};

let NewData = new Object;
var dataTable: InfoTable[];
var keyHandsontable = 'handsontable';
var InfoTable = [
  { ID: 1, name: 'Võ Thị Bích Tuyền', age: '19', sex: 'Female', address: 'Tân Bình', relationship: true},
  { ID: 2, name: 'Phạm Kiều Anh', age: '21', sex: 'Female', address: 'Tân Bình', relationship: true},
  { ID: 3, name: 'Võ Xuân Mạnh', age: '21', sex: 'Male', address: 'Quận 12', relationship: false},
  { ID: 4, name: 'Hoàng Thanh Thảo', age: '21', sex: 'Female', address: 'Bình Thạnh', relationship: false},
  { ID: 5, name: 'Nguyễn Lâm Hà', age: '21', sex: 'Male', address: 'Thủ Đức', relationship: true}
];

@Component({
    selector: 'app-root',
    template:``,
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})

export class AppComponent {
  public autosave = 'false';
  public listAddress = ['Thủ Đức', 'Quận 1', 'Quận 3', 'Quận 4', 'Quận 5', 'Quận 6', 'Quận 7', 'Quận 8', 'Quận 10', 'Quận 11', 'Quận 12', 'Tân Bình', 'Bình Thạnh', 'Gò Vấp', 'Phú Nhuận', 'Nhà Bè', 'Bình Tân', 'Tân Phú', 'Bình Chánh', 'Cần Giờ', 'Củ Chi', 'Hóc Môn']

  consoleData() {
    document.querySelector('#output')!.innerHTML = "Data saved in SessionStorage!"
    setTimeout(() => {
      document.querySelector('#output')!.innerHTML = "Click \"Save data\" to save this data in SessionStorage!"
    }, 5000)
  }

  saveData() {
    localStorage.setItem(keyHandsontable, JSON.stringify(this.dataset));
  }

  dataset: InfoTable[] = localStorage.getItem(keyHandsontable) === null ? InfoTable : JSON.parse(localStorage.getItem(keyHandsontable) || '{}') 

  hotSettings: Handsontable.GridSettings = {
    colHeaders: true,
    rowHeaders: true,
    contextMenu: 
    {
      items: {
        'row_above': {
          name: 'Insert a row above me!'
        },
        'row_below': {
          name: 'A row below me!'
        },
        'separator': ContextMenu.SEPARATOR,
        'clear_custom': {
          name: 'Clear all data!',
          callback: function() {
            this.clear();
          }
        }
      }
    }, 
    width: '75%',
    persistentState: true,
    height: 'auto',
    licenseKey: 'non-commercial-and-evaluation',
    className: 'htCenter',
    afterChange: (changes) => {
      sessionStorage.setItem(keyHandsontable, JSON.stringify(this.dataset))
      dataTable = JSON.parse(sessionStorage.getItem(keyHandsontable) || '{}');
      changes?.forEach(([row]) => {
        const id = dataTable[row].ID;
        // console.log(id)
        NewData[id] = JSON.stringify(dataTable[row]);
      })   
      if (document.querySelector('#autosave')?.ariaChecked === 'true') {
        localStorage.setItem(keyHandsontable, JSON.stringify(this.dataset));
      }
      else return;
    },
  }  

  autoSave() {
    if (document.querySelector('#autosave')?.ariaChecked === 'true') {
      this.autosave = 'false';
    }
    else this.autosave = 'true';
    document.querySelector('#autosave')!.ariaChecked = this.autosave;
    if (this.autosave === 'true') {
      document.querySelector('#output')!.innerHTML = "Data will auto save!"
    }
    else {
      document.querySelector('#output')!.innerHTML = "Data will not auto save!"
      setTimeout(() => {
        document.querySelector('#output')!.innerHTML = "Click \"Save data\" to save this data in localStorage!"}
        , 3000)
    }
  }

  getData() {
    console.log(NewData);
    NewData = {}
  }
}
